//
//  RTExchanger.m
//  Revolut
//
//  Created by Pups on 2/23/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTExchanger.h"

#import "RTCurrencyModel.h"

@interface RTExchanger ()

//@property (nonatomic, strong) NSArray<RTCurrencyModel *> *rt_allRates;
@property (nonatomic, strong) NSArray<RTCurrencyModel *> *rt_definedRates;

@end

static NSUInteger const RT_DEFAULT_AMOUNT = 100;

static NSString *const RT_MAPPING_LABEL_CURRENCY = @"currency";
static NSString *const RT_MAPPING_LABEL_RATE = @"rate";

@implementation RTExchanger

- (void)composeWithRowRates:(NSArray<NSDictionary *> *)rowRates {
    // Simple DEV 'validation'
    if (self.rt_definedRates.count > 1) {
        for (NSDictionary *dictionary in rowRates) {
            NSUInteger index = [self.rt_definedRates indexOfObjectPassingTest:^BOOL(RTCurrencyModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                return [obj.currency isEqualToString:dictionary[RT_MAPPING_LABEL_CURRENCY]];
            }];
            
            if (index == NSNotFound) {
                //NSCAssert(NO, @"Not implemented"); // TODO create new entity if needed
            }
            else {
                self.rt_definedRates[index].rate = [dictionary[RT_MAPPING_LABEL_RATE] doubleValue];
            }
        }
    }
    else {
        RTCurrencyModel *defaultCurrencyModel = self.rt_defaultCurrencyModel;
        
        //NSMutableArray *allRatesStorage = @[defaultCurrencyModel].mutableCopy;
        NSMutableArray *definedRatesStorage = @[defaultCurrencyModel].mutableCopy;
        
        for (NSDictionary *rate in rowRates) {
            RTCurrencyModel *currencyModel = [self rt_currencyModelWithDictionary:rate];
            //[allRatesStorage addObject:currencyModel];
            
            if (currencyModel.currencyCode != RTCurrencyCodeUndefined) {
                [definedRatesStorage addObject:currencyModel];
            }
        }
        
        //self.rt_allRates = allRatesStorage.copy;
        self.rt_definedRates = definedRatesStorage.copy;
        
        if (self.ratesDidChangeSignal) {
            self.ratesDidChangeSignal(self.rt_definedRates); // TODO send immutable copy
        }
    }
}

- (BOOL)exchangeCurrency:(RTCurrencyModel *)currency
              toCurrency:(RTCurrencyModel *)toCurrency
                  amount:(NSUInteger)amount {
    if (currency.amount >= amount) {
        double toAmount = currency.rate * (double)amount / toCurrency.rate;
        
        if (toCurrency.amount >= toAmount) {
            currency.amount -= amount;
            toCurrency.amount += toAmount;
            
            if (self.ratesDidChangeSignal) {
                self.ratesDidChangeSignal(self.rt_definedRates); // TODO immutable send copy
            }
        }
        else {
            return NO;
        }
    }
    else {
        return NO;
    }
    
    return YES;
}

#pragma mark - Private methods

- (RTCurrencyModel *)rt_defaultCurrencyModel {
    RTCurrencyModel *currencyModel = [RTCurrencyModel new];
    currencyModel.currency = [RTCurrencyModel currencyWithCurrencyCode:RTCurrencyCodeEUR];
    currencyModel.amount = RT_DEFAULT_AMOUNT;
    currencyModel.rate = 1;
    
    return currencyModel;
}

- (RTCurrencyModel *)rt_currencyModelWithDictionary:(NSDictionary *)dictionary {
    RTCurrencyModel *currencyModel = [RTCurrencyModel new];
    currencyModel.currency = dictionary[RT_MAPPING_LABEL_CURRENCY];
    currencyModel.amount = RT_DEFAULT_AMOUNT;
    currencyModel.rate = [dictionary[RT_MAPPING_LABEL_RATE] doubleValue];
    
    return currencyModel;
}

@end
