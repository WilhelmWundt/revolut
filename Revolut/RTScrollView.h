//
//  RTScrollView.h
//  Revolut
//
//  Created by Pups on 2/24/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTScrollView : UIScrollView

@property (nonatomic, readonly) NSArray<UIView *> *pages;
@property (nonatomic) NSUInteger index;

- (void)composeWithPages:(NSArray<UIView *> *)pages;

@end
