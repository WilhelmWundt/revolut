//
//  RTCurrencyPageView.m
//  Revolut
//
//  Created by Pups on 2/24/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTCurrencyPageView.h"

#import "RTCurrencyModel.h"

@interface RTCurrencyPageView ()
@property (strong, nonatomic) IBOutlet UILabel *rt_currencyLabel;
@property (strong, nonatomic) IBOutlet UILabel *rt_amountLabel;

@property (strong, nonatomic) IBOutlet UILabel *valueLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;

@end

@implementation RTCurrencyPageView

+ (instancetype)view {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                          owner:nil
                                        options:nil] firstObject];
}

- (void)composeWithCurrency:(RTCurrencyModel *)currency {
    self.rt_currencyLabel.text = currency.currency;
    self.rt_amountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)currency.amount];
}

@end
