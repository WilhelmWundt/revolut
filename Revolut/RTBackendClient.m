//
//  RTBackendClient.m
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTBackendClient.h"

@interface RTBackendClient () <NSXMLParserDelegate>

@property (nonatomic, copy) void (^rt_exchangeRatesHandler)(NSArray<NSDictionary *> *);

@end

@implementation RTBackendClient {
    NSMutableArray *_exchangeRatesStorage;
}

static NSString *const RT_URL_PATH = @"https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

- (void)exchangeRatesWithHandler:(void (^)(NSArray<NSDictionary *> *))handler {
    NSURL *URL = [NSURL URLWithString:RT_URL_PATH];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    __weak typeof(self) weak_self = self;
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"-updateExchangeRates NSURLSessionDataTask response error: %@", error);
        }
        else {
            weak_self.rt_exchangeRatesHandler = handler;
            
            NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
            parser.delegate = weak_self;
            parser.shouldResolveExternalEntities = YES;
            [parser parse];
        }
    }];
    
    [task resume];
}

#pragma mark - <NSXMLParserDelegate>

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    _exchangeRatesStorage = [NSMutableArray new];
}

- (void)parser:(NSXMLParser *)parser
    didStartElement:(NSString *)elementName
       namespaceURI:(NSString *)namespaceURI
      qualifiedName:(NSString *)qName
         attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:@"Cube"] &&
        attributeDict.allKeys.count == 2) { // FIXME ambiguous definition
        [_exchangeRatesStorage addObject:attributeDict];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if (self.rt_exchangeRatesHandler) {
        self.rt_exchangeRatesHandler([_exchangeRatesStorage copy]);
        self.rt_exchangeRatesHandler = nil;
    }
    
    _exchangeRatesStorage = nil;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    _exchangeRatesStorage = nil;
}

@end
