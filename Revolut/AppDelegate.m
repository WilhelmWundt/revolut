//
//  AppDelegate.m
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "AppDelegate.h"

#import "RTGeneralLogic.h"

@interface AppDelegate ()

@property (nonatomic, strong) RTGeneralLogic *generalLogic;

@end

@implementation AppDelegate

- (id)forwardingTargetForSelector:(SEL)aSelector {
    id target = self.generalLogic;
    
    if ([target respondsToSelector:aSelector]) {
        return target;
    }
    else {
        return [super forwardingTargetForSelector:aSelector];
    }
}

- (BOOL)respondsToSelector:(SEL)aSelector {
    if ([super respondsToSelector:aSelector]) {
        return YES;
    }
    else {
        id target = [self forwardingTargetForSelector:aSelector];
        
        if ([target respondsToSelector:aSelector]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Private mutator methods

- (RTGeneralLogic *)generalLogic {
    if (_generalLogic == nil) {
        _generalLogic = [RTGeneralLogic new];
    }
    
    return _generalLogic;
}

@end
