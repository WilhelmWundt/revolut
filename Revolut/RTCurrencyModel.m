//
//  RTCurrencyModel.m
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTCurrencyModel.h"

@interface RTCurrencyModel ()

@property (nonatomic) RTCurrencyCode currencyCode;

@end

static NSString *const RT_CURRENCY_LABEL_EUR = @"EUR";
static NSString *const RT_CURRENCY_LABEL_USD = @"USD";
static NSString *const RT_CURRENCY_LABEL_GBP = @"GBP";

@implementation RTCurrencyModel

+ (NSString *)currencyWithCurrencyCode:(RTCurrencyCode)currencyCode {
    switch (currencyCode) {
        case RTCurrencyCodeEUR:
            return RT_CURRENCY_LABEL_EUR;
        case RTCurrencyCodeUSD:
            return RT_CURRENCY_LABEL_USD;
        case RTCurrencyCodeGBP:
            return RT_CURRENCY_LABEL_GBP;
        case RTCurrencyCodeUndefined:
            return nil;
    }
}

- (void)setCurrency:(NSString *)currency {
    _currency = currency;
    
    self.currencyCode = [self rt_currencyTypeWithString:currency];
}

#pragma mark - Private methods

- (RTCurrencyCode)rt_currencyTypeWithString:(NSString *)string {
    if ([string isEqualToString:RT_CURRENCY_LABEL_EUR]) {
        return RTCurrencyCodeEUR;
    }
    else if ([string isEqualToString:RT_CURRENCY_LABEL_USD]) {
        return RTCurrencyCodeUSD;
    }
    else if ([string isEqualToString:RT_CURRENCY_LABEL_GBP]) {
        return RTCurrencyCodeGBP;
    }
    
    return RTCurrencyCodeUndefined;
}

@end
