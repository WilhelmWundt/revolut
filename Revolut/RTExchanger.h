//
//  RTExchanger.h
//  Revolut
//
//  Created by Pups on 2/23/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RTCurrencyModel;

@interface RTExchanger : NSObject

@property (nonatomic, copy) void (^ratesDidChangeSignal)(NSArray<RTCurrencyModel *> *);

- (void)composeWithRowRates:(NSArray<NSDictionary *> *)rowRates;

- (BOOL)exchangeCurrency:(RTCurrencyModel *)currency
              toCurrency:(RTCurrencyModel *)toCurrency
                  amount:(NSUInteger)amount;
@end
