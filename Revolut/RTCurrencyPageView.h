//
//  RTCurrencyPageView.h
//  Revolut
//
//  Created by Pups on 2/24/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTCurrencyModel;

@interface RTCurrencyPageView : UIView

@property (nonatomic, readonly) UILabel *valueLabel;
@property (nonatomic, readonly) UILabel *rateLabel;

+ (instancetype)view;

- (void)composeWithCurrency:(RTCurrencyModel *)currency;

@end
