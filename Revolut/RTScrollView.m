//
//  RTScrollView.m
//  Revolut
//
//  Created by Pups on 2/24/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTScrollView.h"

@interface RTScrollView ()

@property (nonatomic, strong) NSArray<UIView *> *pages;

@end

static NSUInteger const RT_DEFAULT_INDEX = 1;

@implementation RTScrollView {
    CGRect _defaultVisibleRect;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.showsHorizontalScrollIndicator = NO;
    }
    
    return self;
}

- (void)composeWithPages:(NSArray<UIView *> *)pages {
    self.pages = pages;
    
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    CGFloat abscissa = 0;
    
    for (UIView *pageView in self.pages) {
        pageView.frame = (CGRect){abscissa, 0, self.bounds.size};
        abscissa = CGRectGetMaxX(pageView.frame);
        
        [self addSubview:pageView];
    }
    
    self.contentSize = CGSizeMake(abscissa, CGRectGetHeight(self.bounds));
    
    _defaultVisibleRect = (CGRect){CGRectGetWidth(self.bounds), 0, self.bounds.size};
    self.index = RT_DEFAULT_INDEX;
}

- (void)setIndex:(NSUInteger)index {
    _index = index;
    
    [self scrollRectToVisible:_defaultVisibleRect animated:NO];
}

@end
