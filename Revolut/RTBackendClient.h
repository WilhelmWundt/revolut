//
//  RTBackendClient.h
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTBackendClient : NSObject

- (void)exchangeRatesWithHandler:(void (^)(NSArray<NSDictionary *> *))handler;

@end
