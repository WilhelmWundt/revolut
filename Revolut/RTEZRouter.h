//
//  RTEZRouter.h
//  Revolut
//
//  Created by Pups on 2/23/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class RTExchanger;

@interface RTEZRouter : NSObject

@property (nonatomic, weak) RTExchanger *exchanger;

- (instancetype)initWithExchanger:(RTExchanger *)exchanger;

@end
