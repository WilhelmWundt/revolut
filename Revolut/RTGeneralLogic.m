//
//  RTGeneralLogic.m
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTGeneralLogic.h"
#import "RTBackendClient.h"
#import "RTExchanger.h"
#import "RTEZRouter.h"

#import <UIKit/UIKit.h>

@interface RTGeneralLogic ()

@property (nonatomic, strong) RTBackendClient *backendClient;
@property (nonatomic, strong) RTExchanger *exchanger;

@property (nonatomic, strong) RTEZRouter *rt_ezRouter;

@end

static double const RT_TIMER_INTERVAL = 30.;

@implementation RTGeneralLogic {
    dispatch_source_t _timer;
}

#pragma mark - <UIApplicationDelegate>

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.rt_ezRouter = [[RTEZRouter alloc] initWithExchanger:self.exchanger];
    [self rt_updateExchangeRates];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (_timer) {
        dispatch_source_cancel(_timer);
        
        _timer = nil;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, RT_TIMER_INTERVAL * NSEC_PER_SEC), RT_TIMER_INTERVAL * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        __weak typeof(self) weak_self = self;
        dispatch_source_set_event_handler(timer, ^{
            [weak_self rt_updateExchangeRates];
        });
        dispatch_resume(timer);
    }
    
    _timer = timer;
}

#pragma mark - Private methods

- (void)rt_updateExchangeRates {
    __weak typeof(self) weak_self = self;
    [self.backendClient exchangeRatesWithHandler:^(NSArray<NSDictionary *> *rowRates) {
        [weak_self.exchanger composeWithRowRates:rowRates];
    }];
}

#pragma mark - Private mutator methods

- (RTBackendClient *)backendClient {
    if (_backendClient == nil) {
        _backendClient = [RTBackendClient new];
    }
    
    return _backendClient;
}

- (RTExchanger *)exchanger {
    if (_exchanger == nil) {
        _exchanger = [RTExchanger new];
    }
    
    return _exchanger;
}

@end
