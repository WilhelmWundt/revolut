//
//  ViewController.h
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RTCurrencyModel;

@interface ViewController : UIViewController

@property (nonatomic, copy) void (^exchangeDidInitiateSignal)(RTCurrencyModel *, RTCurrencyModel *, NSUInteger);

- (void)composeWithCurrencies:(NSArray<RTCurrencyModel *> *)currencies;

@end

