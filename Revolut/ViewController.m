//
//  ViewController.m
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "ViewController.h"

#import "RTScrollView.h"
#import "RTCurrencyPageView.h"

#import "RTCurrencyModel.h"

@interface ViewController () <UIScrollViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *rt_exchangeButton;
@property (strong, nonatomic) IBOutlet RTScrollView *rt_fromCurrencyScrollView;
@property (strong, nonatomic) IBOutlet RTScrollView *rt_toCurrencyScrollView;
@property (strong, nonatomic) IBOutlet UITextField *rt_textField;

/*
 * H:[rt_toCurrencyScrollView]-|
 */
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rt_scrollViewBottomConstraint;

@property (nonatomic, strong) NSArray<RTCurrencyModel *> *rt_currencies;

@end

static NSUInteger const RT_MAX_PAGES_COUNT = 3;

@implementation ViewController

- (void)composeWithCurrencies:(NSArray<RTCurrencyModel *> *)currencies {
    self.rt_currencies = currencies;
    
    if (self.isViewLoaded) {
        [self rt_reloadPagesForScrollView:self.rt_fromCurrencyScrollView];
        [self rt_reloadPagesForScrollView:self.rt_toCurrencyScrollView];
        
        [self rt_composeExchangeValidation];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.rt_currencies.count > 0) {
        [self rt_composeScrollViews];
    }
    
    [self.rt_textField becomeFirstResponder];
}

#pragma mark - Private methods

- (IBAction)rt_exchangeButtonDidPress:(id)sender {
    if (self.exchangeDidInitiateSignal) {
        self.exchangeDidInitiateSignal([self rt_currentCurrencyModelForScrollView:self.rt_fromCurrencyScrollView],
                                       [self rt_currentCurrencyModelForScrollView:self.rt_toCurrencyScrollView],
                                       self.rt_textField.text.integerValue);
    }
}

- (IBAction)rt_textFieldEditingChanged:(UITextField *)sender {
    [self rt_currentPageForScrollView:self.rt_fromCurrencyScrollView].valueLabel.text = sender.text;
    [self rt_composeExchangeValue];
    [self rt_composeExchangeValidation];
}

- (void)rt_composeScrollViews {
    [self rt_composeScrollView:self.rt_fromCurrencyScrollView];
    [self rt_composeScrollView:self.rt_toCurrencyScrollView];
    
    [self rt_composeRateDependency];
}

- (void)rt_composeScrollView:(RTScrollView *)scrollView {
    NSUInteger pagesCount = MIN(self.rt_currencies.count, RT_MAX_PAGES_COUNT);
    
    NSMutableArray *storage = [[NSMutableArray alloc] initWithCapacity:pagesCount];
    
    for (NSUInteger i = 0; i < pagesCount; i++) {
        RTCurrencyPageView *pageView = [RTCurrencyPageView view];
        [pageView composeWithCurrency:self.rt_currencies[i]];
        [storage addObject:pageView];
    }
    
    [scrollView composeWithPages:[storage copy]];
}

- (void)rt_reloadPagesForScrollView:(RTScrollView *)scrollView {
    if (scrollView.pages.count == self.rt_currencies.count) {
        for (NSUInteger i = 0; i < scrollView.pages.count - 1; i++) {
            [(RTCurrencyPageView *)scrollView.pages[i] composeWithCurrency:self.rt_currencies[MIN(scrollView.index + i - 1, RT_MAX_PAGES_COUNT - 1)]];
        }
    }
    else {
        [self rt_composeScrollViews];
    }
}

- (void)rt_composeRateDependency {
    RTCurrencyModel *fromCurrencyModel = [self rt_currentCurrencyModelForScrollView:self.rt_fromCurrencyScrollView];
    RTCurrencyModel *toCurrencyModel = [self rt_currentCurrencyModelForScrollView:self.rt_toCurrencyScrollView];
    
    [self rt_currentPageForScrollView:self.rt_toCurrencyScrollView].rateLabel.text =
        [NSString stringWithFormat:@"%.3f %@ = %.3f %@", toCurrencyModel.rate, toCurrencyModel.currency,
                                                         fromCurrencyModel.rate, fromCurrencyModel.currency];
}

- (void)rt_composeExchangeValue {
    [self rt_currentPageForScrollView:self.rt_toCurrencyScrollView].valueLabel.text = [NSString stringWithFormat:@"%.2f", [self rt_exchangeValue]];
}

- (double)rt_exchangeValue {
    double fromValue = [self rt_currentCurrencyModelForScrollView:self.rt_fromCurrencyScrollView].rate;
    double toValue = [self rt_currentCurrencyModelForScrollView:self.rt_toCurrencyScrollView].rate;
    
    return fromValue * self.rt_textField.text.doubleValue / toValue;
}

- (void)rt_composeExchangeValidation {
    UIColor *defaultColor = [UIColor blackColor];
    
    [self rt_currentPageForScrollView:self.rt_fromCurrencyScrollView].valueLabel.textColor = defaultColor;
    [self rt_currentPageForScrollView:self.rt_toCurrencyScrollView].valueLabel.textColor = defaultColor;
    self.rt_exchangeButton.enabled = YES;
    
    UIColor *errorColor = [UIColor redColor];
    
    if ([self rt_currentCurrencyModelForScrollView:self.rt_fromCurrencyScrollView].amount <
        self.rt_textField.text.doubleValue) {
        [self rt_currentPageForScrollView:self.rt_fromCurrencyScrollView].valueLabel.textColor = errorColor;
        self.rt_exchangeButton.enabled = NO;
    }
    
    if ([self rt_currentCurrencyModelForScrollView:self.rt_toCurrencyScrollView].amount <
             [self rt_exchangeValue]) {
        [self rt_currentPageForScrollView:self.rt_toCurrencyScrollView].valueLabel.textColor = errorColor;
        self.rt_exchangeButton.enabled = NO;
    }
}

- (RTCurrencyPageView *)rt_currentPageForScrollView:(RTScrollView *)scrollView {
    return (RTCurrencyPageView *)scrollView.pages[1];
}

- (RTCurrencyModel *)rt_currentCurrencyModelForScrollView:(RTScrollView *)scrollView {
    return self.rt_currencies[scrollView.index];
}

#pragma mark - <UIScrollViewDelegate>

// Simple implementation of infinite scrollView
- (void)scrollViewDidEndDecelerating:(RTScrollView *)scrollView {
    NSCAssert(scrollView.pages.count == RT_MAX_PAGES_COUNT, @"RTScrollView pages count must be equal to RT_MAX_PAGES_COUNT");
    
    NSUInteger index = scrollView.index;
    
    if (scrollView.contentOffset.x > scrollView.frame.size.width) {
        RTCurrencyPageView *pageView = (RTCurrencyPageView *)scrollView.pages[0];
        [pageView composeWithCurrency:self.rt_currencies[index]];
        index = (index >= [self.rt_currencies count] - 1) ? 0 : index + 1;
        pageView = (RTCurrencyPageView *)scrollView.pages[1];
        [pageView composeWithCurrency:self.rt_currencies[index]];
        pageView = scrollView.subviews[2];
        [pageView composeWithCurrency:self.rt_currencies[(index >= [self.rt_currencies count] - 1) ? 0 : index + 1]];
    }
    else if (scrollView.contentOffset.x < scrollView.frame.size.width) {
        RTCurrencyPageView *pageView = (RTCurrencyPageView *)scrollView.pages[2];
        [pageView composeWithCurrency:self.rt_currencies[index]];
        index = (index == 0) ? [self.rt_currencies count] - 1 : index - 1;
        pageView = (RTCurrencyPageView *)scrollView.pages[1];
        [pageView composeWithCurrency:self.rt_currencies[index]];
        pageView = (RTCurrencyPageView *)scrollView.pages[0];
        [pageView composeWithCurrency:self.rt_currencies[(index == 0) ? [self.rt_currencies count] - 1 : index - 1]];
    }
    
    scrollView.index = index;
    
    [self rt_composeRateDependency];
    [self rt_composeExchangeValue];
    [self rt_composeExchangeValidation];
}

@end
