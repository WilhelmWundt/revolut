//
//  RTEZRouter.m
//  Revolut
//
//  Created by Pups on 2/23/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import "RTEZRouter.h"
#import "RTExchanger.h"

#import "ViewController.h"

@implementation RTEZRouter

- (instancetype)initWithExchanger:(RTExchanger *)exchanger {
    self = [super init];
    
    if (self) {
        self.exchanger = exchanger;
    }
    
    return self;
}

- (void)setExchanger:(RTExchanger *)exchanger {
    _exchanger = exchanger;
    _exchanger.ratesDidChangeSignal = ^(NSArray<RTCurrencyModel *> *currencies){
        UIViewController *controller = [[UIApplication sharedApplication] delegate].window.rootViewController;
        
        if (controller.class == ViewController.class) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [(ViewController *)controller composeWithCurrencies:currencies];
            });
            
            [(ViewController *)controller setExchangeDidInitiateSignal:^(RTCurrencyModel *from, RTCurrencyModel *to, NSUInteger value) {
                NSCAssert([exchanger exchangeCurrency:from toCurrency:to amount:value], @"Invalid operation"); // TODO handle the error
            }];
        }
    };
}

@end
