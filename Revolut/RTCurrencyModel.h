//
//  RTCurrencyModel.h
//  Revolut
//
//  Created by Pups on 2/22/17.
//  Copyright © 2017 Serenity. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RTCurrencyCode) {
    RTCurrencyCodeUndefined = -1,
    RTCurrencyCodeEUR,
    RTCurrencyCodeUSD,
    RTCurrencyCodeGBP
};

@interface RTCurrencyModel : NSObject

@property (nonatomic) NSString *_Nullable currency;
@property (nonatomic, readonly) RTCurrencyCode currencyCode;
#warning RTCurrency - Integer value
    @property (nonatomic) NSUInteger amount;
@property (nonatomic) double rate;

+ (NSString *_Nullable)currencyWithCurrencyCode:(RTCurrencyCode)currencyCode;

@end
